steps = [
    [
        """
        CREATE TABLE decks (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(255) NOT NULL,
            category VARCHAR(250) NOT NULL,
            description TEXT
        );
        """,
        """
        DROP TABLE decks;
        """
    ]
]
