#### ACCOUNTS

#### Log in
* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * email: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```
#### Log out
* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```
#### Create User
* Endpoint path: /users
* Endpoint method: POST

* Response: User information
* Response shape:
    ```json
    {
      "user": [
        {
          "name": string,
          "email": string,
          "password": string
        }
      ]
    }
    ```
#### Update user
* Endpoint path: /users/{user_id}
* Endpoint method: PUT
* Response: Updated user information
* Response shape:
    ```json
    {
      "user": [
        {
          "name": string,
          "email": string,
          "password": string
        }
      ]
    }
    ```
#### OPTIONAL - Delete user
* Endpoint: /users/{user_id}
* Endpoint method: DELETE
* Headers:
  * Authorization: Bearer Token
* Response:
Success/Error
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```



#### DECKS
#### Create Deck
* Endpoint path: /api/decks
* Endpoint method: POST
* Headers:
  * Authorization: Bearer Token
* Response: Deck details
* Response shape:
    ```json
    {
      "deck": [
        {
          "title": string,
          "description": string
        }
      ]
    }
#### Get Deck:
* Endpoint method: /api/decks/{deck_id}
* Headers:
  * Authorization: Bearer Token
* Response: Deck details
* Response shape:
    ```json
    {
      "deck": [
        {
          "title": string,
          "category": string,
          "description": string
        }
      ]
    }

#### Get All Decks
* Endpoint path: /api/decks
* Endpoint method: GET
* Headers:
  * Authorization: Bearer Token
* Response: List of Decks
* Response shape:
    ```json
    {
      "decks": [
        {
          "title": string,
          "category": string,
          "description": string
        }
      ]
    }
#### Update Deck
* Endpoint path: /api/decks/{deck_id}
* Endpoint method: PUT
* Headers:
  * Authorization: Bearer Token
* Response: Updated Deck details
* Response shape:
    ```json
    {
      "deck": [
        {
          "title": string,
          "category": string, 
          "description": string
        }
      ]
    }
#### Delete Deck
* Endpoint path: /api/decks/{deck_id}
* Endpoint method: DELETE
* Headers:
  * Authorization: Bearer Token
* Response:
Success/Error
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```



#### CARDS
#### Create Card
* Endpoint path: /api/cards
* Endpoint method: POST
* Headers:
  * Authorization: Bearer Token
* Request Shape:
```json
    {
      "card": [
        {
          "question": string,
          "answer": string,
          "correct": bool (auto:false),
          "deck":{
            "deck_id": int,
          },
        }
      ]
    }
```
* Response:
Success/Error
* Response shape:
```json
    {
      "success": boolean,
      "message": string
    }
  ```

#### Get Card
* Endpoint path: /api/cards/{deck_id}/{card_id}
* Endpoint method: GET
* Headers:
  * Authorization: Bearer Token
* Request Shape:
//
* Response:
A specific card, by card ID.
* Response shape:
```json
    {
      "card": [
        {
          "question": string,
          "answer": string,
          "correct": bool (auto:false),
          "deck":{
            "deck_id": int,
          },
        }
      ]
    }
```

#### Get All Cards From Specific Deck
* Endpoint path: /api/cards/{deck_id}
* Endpoint method: GET
* Headers:
  * Authorization: Bearer Token
* Request Shape:
//
* Response:
A list of all cards, dependent on deck_id
* Response shape:
```json
    {
      "cards": [
        {
          "question": string,
          "answer": string,
          "correct": bool,
          "deck":{
            "deck_id": int,
          },
        }
      ]
    }
```

#### Update Card
* Endpoint path: /api/cards/{card_id}
* Endpoint method: PUT
* Headers:
  * Authorization: Bearer Token
* Request Shape:
//
* Response:
A specific card, by card ID
* Request Shape:
```json
  {
    "question":string,
    "answer": string,
    "deck":{
      "deck_id": int,
  }
  }
```
* Response shape:
```json
    {
      "card": [
        {
          "question": string,
          "answer": string,
          "correct": bool,
          "deck":{
            "deck_id": int,
          },
        }
      ]
    }
```

#### Delete card
* Endpoint path: /api/cards/{card_id}
* Endpoint method: DELETE
* Headers:
  * Authorization:
* Request Shape:
//
* Response:
Success/Error
* Response shape:
    ```json
    {
      "success": boolean,
      "message": string
    }
    ```
