
##MOCK MODEL
from pydantic import BaseModel

class Card(BaseModel):
    question: str
    answer: str
    correct: boolean, default: false
    deck: Deck** (acts as FK)



##MOCK GET REQUEST
from fastapi import FastApi
from models import Card
from db import CardQueries as CardQ

app = FastAPI()

@app.get("/api/cards/{card_id}", response_model=Card)
def card_details(card_id: int):
    card = CardQ.get_card(card_id)
    return card




##MOCK POST REQUEST
from fastapi import FastApi
from models import Card
from db import CardQueries as CardQ

app = FastAPI()
cool

@app.post("/api/cards")
def card_details(card: Card):
    card = CardQ.create_card(card)
    return card






TO SET UP MIGRATIONS...
In migrations, create file: 001_create_table.py (or whatever, but number it)
